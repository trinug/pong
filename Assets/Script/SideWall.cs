﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideWall : MonoBehaviour {

	[SerializeField]
	private GameManager gameManager;

	public PlayerControll player;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D anotherCollider) {
		if (anotherCollider.name == "Ball") {
			player.IncrementScore ();
			if (player.Score < gameManager.maxScore) {
				anotherCollider.gameObject.SendMessage ("RestartGame", 2.0f, SendMessageOptions.RequireReceiver);
			}
		}
	}
}
